import pandas as pd
import joblib

# Load the fraud detection model
model = joblib.load("modele_fraud_detection.joblib")

# Prétraitement des données CSV pour définir les colonnes du DataFrame cible
df = pd.read_csv("./fraud_oracle/fraud_oracle.csv")
df = df.drop(columns="PolicyNumber")
df.Age = df.Age.replace(0, 16)
mask_holder2 = df["AgeOfPolicyHolder"] != "16 to 17"
df = df[mask_holder2]
df = df.drop(columns="PolicyType")
df = pd.get_dummies(df, columns=['Month', 'WeekOfMonth', 'DayOfWeek', 'Make', 'AccidentArea', 'DayOfWeekClaimed', 'MonthClaimed',
                                 'WeekOfMonthClaimed', 'Sex', 'MaritalStatus', 'Fault', 'VehicleCategory', 'VehiclePrice', 'RepNumber',
                                 'DriverRating', 'Days_Policy_Accident', 'Days_Policy_Claim', 'PastNumberOfClaims', 'AgeOfVehicle',
                                 'AgeOfPolicyHolder', 'PoliceReportFiled', 'WitnessPresent', 'AgentType', 'NumberOfSuppliments',
                                 'AddressChange_Claim', 'NumberOfCars', 'Year', 'BasePolicy'],
                    drop_first=True)

X = df.drop(columns=['FraudFound_P'])

# Créer un DataFrame avec les mêmes colonnes que X mais avec des valeurs nulles
dataframe_zero = pd.DataFrame(0, index=range(len(X)), columns=X.columns)

def preprocess_data(data):
    # Data preprocessing logic here
    df = pd.DataFrame([data], columns=["Month", "WeekOfMonth", "DayOfWeek", "Make", "AccidentArea", "DayOfWeekClaimed", "MonthClaimed", "WeekOfMonthClaimed", "Sex","MaritalStatus","Age",
                                       "Fault", "VehicleCategory", "VehiclePrice" , "RepNumber", "Deductible", "DriverRating", "Days_Policy_Accident", "Days_Policy_Claim", "PastNumberOfClaims", "AgeOfVehicle",
                                       "AgeOfPolicyHolder", "PoliceReportFiled", "WitnessPresent","AgentType",  "NumberOfSuppliments", "AddressChange_Claim", "NumberOfCars", "Year", "BasePolicy"])

    df = df.drop(columns="PolicyNumber", errors='ignore')
    df.Age = df.Age.replace(0, 16)
    df = df[df["AgeOfPolicyHolder"] != "16 to 17"]
    df = df.drop(columns="PolicyType", errors='ignore')

    df = pd.get_dummies(df, columns=['Month', 'WeekOfMonth', 'DayOfWeek', 'Make', 'AccidentArea', 'DayOfWeekClaimed', 'MonthClaimed',
                                     'WeekOfMonthClaimed', 'Sex', 'MaritalStatus', 'Fault', 'VehicleCategory', 'VehiclePrice', 'RepNumber',
                                     'DriverRating', 'Days_Policy_Accident', 'Days_Policy_Claim', 'PastNumberOfClaims', 'AgeOfVehicle',
                                     'AgeOfPolicyHolder', 'PoliceReportFiled', 'WitnessPresent', 'AgentType', 'NumberOfSuppliments',
                                     'AddressChange_Claim', 'NumberOfCars', 'Year', 'BasePolicy'], drop_first=True)

    # Assurez-vous que toutes les colonnes nécessaires sont présentes dans df
    df = df.reindex(columns=X.columns, fill_value=0)

    return df

def predict_fraud(data):
    preprocessed_data = preprocess_data(data)
    prediction = model.predict(preprocessed_data)
    return prediction
