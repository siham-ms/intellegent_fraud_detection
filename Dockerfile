FROM python:3.9-slim

RUN apt-get update && apt-get install -y libgomp1

WORKDIR /python-docker

COPY requirement.txt requirement.txt
RUN pip install -r requirement.txt

COPY . .
RUN pip install --upgrade pip && pip install --upgrade lightgbm

ENV FLASK_APP=app.py


CMD ["gunicorn", "--bind", "0.0.0.0:5000", "app:app"]