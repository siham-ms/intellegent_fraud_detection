Flask==2.0.3
mysql-connector-python==8.0.26
pandas==1.3.3
joblib==1.4.2
gunicorn==20.1.0
Werkzeug==2.0.3
lightgbm==3.2.1
python-dotenv==0.19.2

