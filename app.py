from flask import Flask, render_template, request, flash, jsonify
import mysql.connector
import os
from modele import predict_fraud
from dotenv import load_dotenv
load_dotenv()

app = Flask(__name__)
app.secret_key = "tawfiqsudine212"

# Connect to MySQL database
conn = mysql.connector.connect(
    host=os.environ.get("DB_HOST"),
    user=os.environ.get("DB_USER"),
    password=os.environ.get("DB_PASSWORD"),
    database=os.environ.get("DB_NAME"),
    port=os.environ.get("PORT")
)
cursor = conn.cursor()

@app.route("/verify")
def index():
    flash("Auto Insurance Fraud Detection")
    return render_template("index.html")

@app.route("/check", methods=["POST", "GET"])
def fraud_test():
    client_id = request.form['input_id']

    cursor.execute('''
        SELECT Month, WeekOfMonth, DayOfWeek, Make, AccidentArea, DayOfWeekClaimed, MonthClaimed, WeekOfMonthClaimed, Sex,MaritalStatus,Age,
                Fault, VehicleCategory, VehiclePrice , RepNumber, Deductible, DriverRating, Days_Policy_Accident, Days_Policy_Claim, PastNumberOfClaims, AgeOfVehicle,
             AgeOfPolicyHolder, PoliceReportFiled, WitnessPresent,AgentType,  NumberOfSuppliments, AddressChange_Claim, NumberOfCars, Year, BasePolicy FROM dbreclamation WHERE id = %s''', (client_id,))
    latest_params = cursor.fetchone()

    if latest_params is not None:
        # Convert tuple to dictionary to match the expected input format for preprocessing
        column_names = ["Month", "WeekOfMonth", "DayOfWeek", "Make", "AccidentArea", "DayOfWeekClaimed", "MonthClaimed", "WeekOfMonthClaimed", "Sex","MaritalStatus","Age",
                        "Fault", "VehicleCategory", "VehiclePrice" , "RepNumber", "Deductible", "DriverRating", "Days_Policy_Accident", "Days_Policy_Claim", "PastNumberOfClaims", "AgeOfVehicle",
                        "AgeOfPolicyHolder", "PoliceReportFiled", "WitnessPresent","AgentType",  "NumberOfSuppliments", "AddressChange_Claim", "NumberOfCars", "Year", "BasePolicy"]
        latest_params_dict = dict(zip(column_names, latest_params))

        # Prediction
        prediction = predict_fraud(latest_params_dict)

        if prediction[0] == 1:
            flash(f"The client with id {client_id} is flagged for fraud!")
        else:
            flash(f"No fraud detected for the client with id {client_id}")
    else:
        flash(f"No data found for the client with id {client_id}")

    return render_template("index.html")

@app.route("/fetch_test")
def get_results():
    cursor.execute('SELECT * FROM dbreclamation')
    results = cursor.fetchall()
    result_list = [dict(zip(cursor.column_names, row)) for row in results]
    return jsonify(result_list)

if __name__ == "__main__":
    app.run(debug=True)
